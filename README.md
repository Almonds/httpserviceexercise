<h4> Service to expose user and group information on a unix system

Written in Kotlin with Gradle as the build tool. Doing this
with Spark, currently have GSON in the gradle build file but
I may remove that and do the JSON formatting myself. 

<h4>Dependencies:<h4>

* Apache Spark
* Google GSON
* Junit

<h4>Build:

It is an Intellij project. Open it with Intellij and
run the file with main in it: ServicesMain.kt. A little
green arrow shows up right next to

fun main(args: Array<String>),
click that and click Run 'ServicesMainKt'



Created in Intellij 2018.3.3
<br/>Gradle 4.10
<br/>Kotlin 1.3.20
<br/>Spark 2.3
<br/>Gson 2.8.5
<br/>Junit 5.3.2

<h4>Service URL's:

* http://localhost:4567/users
* http://localhost:4567/users/\<uid>
* http://localhost:4567/users/\<uid>/groups
* http://localhost:4567/users/query?\<param&...>
* http://localhost:4567/groups
* http://localhost:4567/groups/\<gid>
* http://localhost:4567/groups/query?\<param&...>

