package RestHandlers

import com.google.gson.Gson
import dataTypes.Group
import java.io.File
import java.util.*
import spark.Spark.get

object GroupRestHandler {
    private var filePath = "/etc/group"
    private val gson = Gson()
    private const val GROUP_NAME = 0
    private const val GID = 2
    private const val GROUP_LIST = 3

    fun setPath(path: String){
        if(File(path).exists()) filePath = path
        else throw NoSuchFileException(File(path))
    }

    fun startServices(){
        get("/groups", "application/json") { req, res -> GroupRestHandler.groupData() }
        get("/groups/query", "application/json"){req, res ->
            val filters = HashMap<Int, String>()
            req.queryParams("name")?.let { filters.put(GROUP_NAME, it) }
            req.queryParams("gid")?.let { filters.put(GID, it) }
            req.queryParams("members")?.let { filters.put(GROUP_LIST, it) }

            filteredGroupData(filters)
        }
        get("/groups/:gid", "application/json"){req, res ->
            req.params(":gid")?.let { gidGroupData(it)}
        }
    }

    /**
     * Returns all groups from group file
     *
     * @return JsonArray of all groups
     */
    fun groupData(): String{
        val groupJsonArray = ArrayList<Group>()
        File(filePath).forEachLine {
            val group = it.split(":")
            groupJsonArray.add(
                Group(
                    group[GROUP_NAME],
                    group[GID],
                    group[GROUP_LIST]
                )
            )
        }

        return gson.toJson(groupJsonArray)
    }

    /**
     * Returns all groups from group file
     *
     * @param filters name,gid,members: match criteria
     * @return JsonArray of all groups
     */
    private fun filteredGroupData(filters: HashMap<Int, String>): String{
        if(filters.size == 0) return groupData()

        val groups = ArrayList<Group>()
        File(filePath).forEachLine {
            val group = it.split(":")
            var fullMatch = true
            for(key in filters.keys){
                if(group[key] != filters[key]){
                    fullMatch = false
                }
            }
            if(fullMatch){
                groups.add(
                    Group(
                        group[GROUP_NAME],
                        group[GID],
                        group[GROUP_LIST]
                    )
                )
            }

        }

        return gson.toJson(groups)
    }

    /**
     * Returns all groups from group file
     *
     * @param gid to match to a group
     * @return Json string of that group data
     */
    private fun gidGroupData(gid: String): String{
        val file = File(filePath)
        val scanner = Scanner(file)

        while(scanner.hasNextLine()){
            val line = scanner.nextLine().split(":")
            if (line[GID] == gid){
                return gson.toJson(
                    Group(
                        line[GROUP_NAME],
                        line[GID],
                        line[GROUP_LIST]
                    )
                )
            }
        }
        return "GROUP ID($gid) NOT FOUND"
    }
}