package RestHandlers

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import dataTypes.Group
import dataTypes.User
import java.io.File
import java.util.*
import spark.Spark.get
import kotlin.collections.ArrayList

object UserRestHandler {
    private var filePath = "/etc/passwd"
    private val gson = Gson()
    private const val NAME = 0
    private const val UID = 2
    private const val GID = 3
    private const val COMMENT = 4
    private const val HOME = 5
    private const val SHELL = 6

    fun setPath(path: String){
        if(File(path).exists()) filePath = path
        else throw NoSuchFileException(File(path))
    }

    fun startServices(){
        get("/users", "application/json") { req, res -> userData() }
        get("/users/query", "application/json") { req, res ->
            val filters = HashMap<Int, String>()
            req.queryParams("name")?.let { filters.put(NAME, it) }
            req.queryParams("uid")?.let { filters.put(UID, it) }
            req.queryParams("gid")?.let { filters.put(GID, it) }
            req.queryParams("comment")?.let { filters.put(COMMENT, it) }
            req.queryParams("home")?.let { filters.put(HOME, it) }
            req.queryParams("shell")?.let { filters.put(SHELL, it) }

            filteredUserData(filters)
        }
        get("/users/:uid", "application/json"){req, res ->
            req.params(":uid")?.let { uidUserData(it)}
        }
        get("/users/:uid/groups"){req, res ->
            req.params(":uid")?.let { uidUserGroups(it) }
        }
    }

    /**
     * Returns all users from passwd file
     *
     * @return JsonArray of all users
     */
    private fun userData(): String{
        val userJsonArray = ArrayList<User>()
        File(filePath).forEachLine {
            val user = it.split(":")
            userJsonArray.add(
                User(
                    user[NAME],
                    user[UID],
                    user[GID],
                    user[COMMENT],
                    user[HOME],
                    user[SHELL]
                )
            )
        }

        return gson.toJson(userJsonArray)
    }

    /**
     * Grabs a user from the uid, then matches its name against the group members
     *
     * @param filters name,uid,gid,comment,home,shell: match criteria
     * @return JsonArray string of matched users
     */
    private fun filteredUserData(filters: HashMap<Int, String>): String{
        if(filters.size == 0) return userData()
        val users = ArrayList<User>()
        File(filePath).forEachLine {
            val user = it.split(":")
            var fullMatch = true
            for(key in filters.keys){
                if(user[key] != filters[key]){
                    fullMatch = false
                }
            }
            if(fullMatch){
                users.add(
                    User(
                        user[NAME],
                        user[UID],
                        user[GID],
                        user[COMMENT],
                        user[HOME],
                        user[SHELL]
                    ))
            }
        }

        return gson.toJson(users)
    }

    /**
     * Grabs a user from the uid, then matches its name against the group members
     *
     * @param uid The unique id grab User data
     * @return Json string of matched User or string of failure
     */
    private fun uidUserData(uid: String): String{
        val file = File(filePath)
        val scanner = Scanner(file)

        while(scanner.hasNextLine()){
            val user = scanner.nextLine().split(":")
            if (user[UID] == uid){
                return gson.toJson(
                    User(
                        user[NAME],
                        user[UID],
                        user[GID],
                        user[COMMENT],
                        user[HOME],
                        user[SHELL]
                    )
                )
            }
        }
        return "USER ID($uid) NOT FOUND"
    }

    /**
     * Grabs a user from the uid, then matches its name against the group members
     *
     * @param uid The unique id grab User data
     * @return JsonArray string of matched groups for a given user
     */
    private fun uidUserGroups(uid: String): String{
        try{
            val user = gson.fromJson(uidUserData(uid), User::class.java)
            val groups = gson.fromJson(GroupRestHandler.groupData(), Array<Group>::class.java)
            val matchedGroups = ArrayList<Group>()

            groups.forEach {
                // do it this way so not to match substrings
                val memberArray = it.members.split(",")
                for(member in memberArray)
                    if(member.equals(user.name))
                        matchedGroups.add(it)
            }

            return gson.toJson(matchedGroups)
        }
        catch (e: JsonSyntaxException){
            return "USER ID($uid) NOT FOUND"
        }
    }
}
