import RestHandlers.GroupRestHandler
import RestHandlers.UserRestHandler

class ServicesDriver() {
    init {
        start()
    }
    fun start(){
        UserRestHandler.startServices()
        GroupRestHandler.startServices()
    }
}