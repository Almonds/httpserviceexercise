import RestHandlers.GroupRestHandler
import RestHandlers.UserRestHandler

fun main(args: Array<String>){
    if(args.isNotEmpty()){
        if(args.size < 2){
            println("Insufficient Arguments. Please enter users file path <space> group file path")
            System.exit(0)
        }
        UserRestHandler.setPath(args[0])
        GroupRestHandler.setPath(args[1])
    }
    UserRestHandler.startServices()
    GroupRestHandler.startServices()
}


