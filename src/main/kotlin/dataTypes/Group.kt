package dataTypes

import com.google.gson.annotations.SerializedName

data class Group(
    @SerializedName("name") val name: String = "",
    @SerializedName("gid") val gid: String = "",
    @SerializedName("members") val members: String = ""
)