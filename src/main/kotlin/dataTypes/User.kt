package dataTypes

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("name") val name: String = "",
    @SerializedName("uid") val uid: String = "",
    @SerializedName("gid") val gid: String = "",
    @SerializedName("comment") val comment: String = "",
    @SerializedName("home") val home: String = "",
    @SerializedName("shell") val shell: String = ""
)