import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

internal class ServicesDriverTest {
    private val userBaseUrl = "http://localhost:4567/users"
    private val groupBaseUrl = "http://localhost:4567/groups"
    private lateinit var services: ServicesDriver

    @BeforeEach
    fun setUp() {
        services = ServicesDriver()
        Thread.sleep(1000)
    }

    @AfterEach
    fun tearDown() {
    }

    fun printUrlResponse(address: URL){
        with(address.openConnection() as HttpURLConnection){
            requestMethod = "GET"
            assert(responseCode == 200){"Response code: $responseCode"}
            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()
                var line = it.readLine()

                while (line != null) {
                    response.append(line)
                    line = it.readLine()
                }
                println(response.toString())
            }
        }
    }

    @Test
    fun start() {
        printUrlResponse(URL(userBaseUrl))
        printUrlResponse(URL(userBaseUrl+"/0"))
        printUrlResponse(URL(userBaseUrl+"/query?name=daemon"))
        printUrlResponse(URL(userBaseUrl+"/1000/groups"))

        printUrlResponse(URL(groupBaseUrl))
        printUrlResponse(URL(groupBaseUrl+"/0"))
        printUrlResponse(URL(groupBaseUrl+"/query?name=root"))
    }
}